// pages/yinyong/yingyong.js
Page({
    data: {
        activeTab: '最新上线' // 初始激活的选项
      },
      toggleTab(e) {
        const tab = e.currentTarget.dataset.tab;
        if (tab !== this.data.activeTab) {
          this.setData({
            activeTab: tab
          });
        }
      }
});
