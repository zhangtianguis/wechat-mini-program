// pages/faxian/faxian.js
Page({
    data: {
      options: ['通知公告', '学校新闻', '综合信息', '学术动态'],
      selectedOption: '通知公告', // 默认选中的选项
      notices: [
        { type: '通知公告', title: '关于加强新生入学后校园安全管理规范的通知', department: '校园新闻网', image: '/imgs/newfeel.png' },
        { type: '通知公告', title: '关于加强新生入学后校园安全管理规范的通知', department: '校园新闻网', image: '/imgs/newfeel.png' },
        { type: '通知公告', title: '关于加强新生入学后校园安全管理规范的通知', department: '校园新闻网', image: '/imgs/newfeel.png' },
        { type: '学校新闻', title: '关于加强新生入学后校园安全管理规范的通知', department: '校园新闻网', image: '/imgs/newfeel.png' },
        { type: '学校新闻', title: '关于加强新生入学后校园安全管理规范的通知', department: '校园新闻网', image: '/imgs/newfeel.png' },
        { type: '学校新闻', title: '关于加强新生入学后校园安全管理规范的通知', department: '校园新闻网', image: '/imgs/newfeel.png' },
        { type: '综合信息', title: '关于开展2023新生入学教育系列培训课程的指导意见', department: '信息化管理中心', image: '/imgs/item2.png' },
        { type: '综合信息', title: '关于开展2023新生入学教育系列培训课程的指导意见', department: '信息化管理中心', image: '/imgs/item2.png' },
        { type: '综合信息', title: '关于开展2023新生入学教育系列培训课程的指导意见', department: '信息化管理中心', image: '/imgs/item2.png' },
        { type: '学术动态', title: '关于加强新生入学后校园安全管理条例实行的通知', department: '教务处', image: '/imgs/item1.png' },
        { type: '学术动态', title: '关于加强新生入学后校园安全管理条例实行的通知', department: '教务处', image: '/imgs/item1.png' },
        { type: '学术动态', title: '关于加强新生入学后校园安全管理条例实行的通知', department: '教务处', image: '/imgs/item1.png' }
      ]
    },
    selectOption: function(e) {
      let option = e.currentTarget.dataset.item;
      this.setData({
        selectedOption: option
      });
    }
  });